# taskomodoro

Task warrior pomodoro tool

__This is my first Python project, if you think there is a more Pythonic way of doing thing. Please drop me a line on mastodon : sinarf@framapiaf.org, or create an issue.__

## Intention of the project

- having a way to use and track pomodoros in taskwarrior.
- Learning Python with a project that's starting as a script, to learn the basics and have a gui to make it easy to use.

## Features needed

- Implementation in python
- Start the task with the pomodoro and stop at the end
- Handle Ctrl+C as an interruption
- Add metadata in the task to keep trace

## Milestones

- CLI - pure CLI, see patata for inspiration
- gui - interactive notification
- interactive gui - to be launch from Albert

